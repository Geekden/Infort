# Infort

This tool analyzes all files of selected Front-End project in contrast to its analogues, which take only package.json from the project root. 
> If the information is present in the project somewhere, then the algorithm will find it with a **99%** probability.

# Installation

Install with npm:
```
npm install --save infort
```
Install with yarn:
```
yarn add infort
```
# Getting Started

```
const  infort = require('./infort');

infort('C:/Path/To/Front-End/Project')
	.then(info => console.log(info));
```
> This program will display all collected information from the project files.

 It analyzes package.json, bower.json and html files in all subfolders and collects all information from all. Next, it sorts all the collected information by priorities and output the result.
 
 With high accuracy, it finds bundlers, preprocessors, task runners, test runners and other information.

# Maintaining

If you want to help the project, you can replenish the database of tools in the folder **src/data**