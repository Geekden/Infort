'use strict';

var cheerio = require('cheerio');
var parseUrl = require('url-parse');
var searchByKey = require('./searchByKey');
var cdn = require('./data/cdn.json');

/**
 * Search for information from html files.
 * 
 * @function searchInfoHtml
 * @param {String} data - Html data.
 * @return {Object} Information from html file.
 * @property {(String | undefined)} name 
 * @property {(String | undefined)} description
 * @property {(String | undefined)} keywords 
 * @property {(String | undefined)} version 
 * @property {(String | undefined)} author 
 * @property {Array} dependencies 
 */
module.exports = function (data) {
    var info = {
        name: undefined,
        description: undefined,
        keywords: undefined,
        version: undefined,
        author: undefined,
        dependencies: []
    };

    /**
     * Parsed html.
     * @type {Object}
     */
    var $ = cheerio.load(data);

    /** Basic info from special tags
     * @type {(String | undefined)}
     */
    info.name = $("title").text();
    info.description = $('meta[name="description"]').attr("content");
    info.keywords = $('meta[name="keywords"]').attr("content");
    /** Create an array with keywords if not undefined */
    info.keywords = info.keywords ? info.keywords.split(',').map(function (x) {
        return x.trim();
    }) : info.keywords;
    info.version = $('meta[name="version"]').attr("content");
    info.author = $('meta[name="author"]').attr("content");

    /** Dependencies from CDN */
    $('link[rel=stylesheet], script').each(function (i, el) {
        /** Get url from link or script */
        var link = $(el).attr("tagName") == 'LINK' ? $(el).attr('href') : $(el).attr('src');

        /**
         * Parsed url
         * @type {Object}
         */
        var url = parseUrl(link);

        /** Find url and cdn database matches */
        var isCDN = searchByKey(cdn, url.host);

        /** If link is a valid url and found in cdn database */
        if (!link || !/^http.*\.(css|js)$/i.test(link) || !isCDN) return;

        /**
         * Path to CDN file
         * @type {Array}
         */
        var urlPath = url.pathname.split('/');

        /** 
         * Name of CDN library
         * @type {String}
         */
        var name = urlPath.pop().replace(/(\.min)?\.(css|js)$/, '').replace(/\@/g, '');

        /** 
         * Version of CDN library
         * @type {String}
         */
        var version = '';

        /** 
         * Version format 
         * @type {Object}
         */
        var formatVersion = /(\d+\.\d+(\.\d+)?)/;

        /** If the version is in the file name */
        if (formatVersion.test(name)) {
            /** Match and remove version from the name */
            version = name.replace(formatVersion, "$1");
        } else {
            /** Search for version in path to this file */
            urlPath.forEach(function (el) {
                /** Check if the folder corresponding to the format version */
                version = formatVersion.test(el) ? el.match(formatVersion)[0] : version;
            });
        }

        info.dependencies.push({
            name: name,
            version: version,
            url: link
        });
    });

    return info;
};