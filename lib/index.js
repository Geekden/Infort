'use strict';

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

var recursive = require('recursive-readdir');
var detectInfo = require('./detectInfo');

var infort = function () {
    var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(projectPath) {
        var excludeFiles, files, info;
        return regeneratorRuntime.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:

                        /**
                         * Non-project files.
                         * https://github.com/github/gitignore/blob/master/Node.gitignore
                         * 
                         * @type {Array}
                         */
                        excludeFiles = ['node_modules', 'bower_components', 'jspm_packages', 'logs', 'npm-debug.log*', 'yarn-debug.log*', 'yarn-error.log*', 'pids', '*.pid', '*.seed', '*.pid.lock', 'lib-cov', 'coverage', '.nyc_output', '.grunt', '.lock-wscript', 'typings', '.npm', '.eslintcache', '.node_repl_history', '*.tgz', '.yarn-integrity', '.env', '.next', '.vuepress/dist', '.serverless', 'bin'];

                        /**
                         * Recursive list of project file paths.
                         * @type {Array}
                         */

                        _context.next = 3;
                        return recursive(projectPath, excludeFiles);

                    case 3:
                        files = _context.sent;


                        /**
                         * Information about the project.
                         * 
                         * @type {Object}
                         * @property {(String | null)} name - Name of the project.
                         * @property {(String | null)} description - Description of the project.
                         * @property {(String | null)} keywords - Keywords of the project.
                         * @property {(String | null)} version - Version of the project.
                         * @property {(String | null)} icon - Icon of the project.
                         */
                        info = detectInfo(files);
                        return _context.abrupt('return', info);

                    case 6:
                    case 'end':
                        return _context.stop();
                }
            }
        }, _callee, undefined);
    }));

    return function infort(_x) {
        return _ref.apply(this, arguments);
    };
}();

module.exports = infort;