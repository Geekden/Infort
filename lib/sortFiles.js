'use strict';

var path = require('path');
var sortArrayFileFirst = require('./sortArrayFileFirst');
var searchByKey = require('./searchByKey');

var preprocessors = require('./data/preprocessors.json');
var backend = require('./data/backend.json');

/**
 * Sort project files by categories for analyze.
 * 
 * @function findInfoHtml
 * @param {Array} files - Project files.
 * @return {Object} Sorted files.
 * @property {Array} package - Found package.json files.
 * @property {Array} html - Found html files.
 * @property {Array} bower - Found bower.json files.
 * @property {Array} images - Found images.
 */
module.exports = function (files) {
    var fileTypes = {
        package: [],
        html: [],
        bower: [],
        images: [],
        found: {
            preprocessors: [],
            backend: []
        }
    };

    /**
     * Sort an array by file type.
     * 
     * @param {String} filePath - Path to project file.
     */
    files.forEach(function (filePath) {
        var file = path.parse(filePath);

        /**
         * Filename from path of the file in lowercase.
         * @type {String}
         */
        var fileName = file.name.toLowerCase();

        /**
         * File extension from path of the file in lowercase without dot at start.
         * @type {String}
         */
        var fileExt = file.ext.toLowerCase();

        /** Sort files. */
        if (sortFile(fileName, fileExt, 'package', '.json')) fileTypes.package.push(filePath);
        if (sortFile(fileName, fileExt, false, '.html')) fileTypes.html.push(filePath);
        if (sortFile(fileName, fileExt, 'bower', '.json')) fileTypes.bower.push(filePath);
        if (sortFile(fileName, fileExt, false, ['.ico', '.jpg', '.jpeg', '.png', '.svg'])) fileTypes.images.push(filePath);

        /** Search preprocessors by filename */
        var preprocessor = searchByKey(preprocessors.file, fileName + fileExt);
        if (preprocessor) {
            fileTypes.found.preprocessors.push(preprocessor);
        }

        /** Search preprocessors by extension */
        preprocessor = searchByKey(preprocessors.ext, fileExt);
        if (preprocessor) {
            fileTypes.found.preprocessors.push(preprocessor);
        }

        /** Search back-end languages */
        var backendLang = searchByKey(backend, fileExt);
        if (backendLang) {
            fileTypes.found.backend.push(backendLang);
        }
    });

    /**
     * Checks if the file coincides with the description.
     * 
     * @function sortFile
     * @param {String} fileName
     * @param {String} fileExt
     * @param {String} sortName - Proper Name.
     * @param {String} sortExt - Proper Extension.
     * @return {Boolean}
     */
    function sortFile(fileName, fileExt, sortName, sortExt) {
        /** If the full name is entered */
        if (sortName && sortExt && fileName + fileExt === sortName + sortExt) {
            return true;
        }
        /** If only the name is entered */
        else if (sortName && !sortExt && fileName === sortName) {
                return true;
            }
            /** If only the extension is entered */
            else if (!sortName && sortExt) {
                    if (typeof sortExt == 'string' && fileExt === sortExt || Array.isArray(sortExt) && sortExt.some(function (el) {
                        return el === fileExt;
                    })) {
                        return true;
                    }
                }
                /** If the file does not match the condition */
                else {
                        return false;
                    }
    }

    /** Remove undefined values from all arrays */
    //Object.keys(fileTypes).forEach((key) => fileTypes[key].filter(Boolean));

    /** Sort html files. Index.html firstly.*/
    fileTypes.html = sortArrayFileFirst(fileTypes.html, /index\.html/i);

    return fileTypes;
};