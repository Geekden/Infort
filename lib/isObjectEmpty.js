"use strict";

/**
 * Check if object has only null properties
 * 
 * @function isObjectEmpty
 * @param {Object} obj - Object.
 * @return {*}
 */
module.exports = function (obj) {
  return Object.values(obj).every(function (x) {
    return !x;
  }) || Object.keys(obj).every(function (key) {
    return !obj.hasOwnProperty(key);
  });
};