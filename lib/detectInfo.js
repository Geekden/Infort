'use strict';

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

var uniqArr = require('./uniqArr');

var sortFiles = require('./sortFiles');

var searchInfo = require('./searchInfo');
var searchInfoHtml = require('./searchInfoHtml');
var searchInfoPackage = require('./searchInfoPackage');
var searchIcons = require('./searchIcons');

/**
 * Collects all information about the project.
 * 
 * @function detectInfo
 * @param {Array} files - Paths of project files.
 * @return {Object} Full information about the project.
 */
module.exports = function (files) {
    /**
     * Sorted files. It is necessary not to read unnecessary files. Optimization.
     * 
     * @type {Object}
     */
    var fileTypes = sortFiles(files);

    /**
     * Сollected information. If object is empty or files not found, return empty array.
     * 
     * @type {Object}
     * @property {Array} package - Found information from package.json files.
     * @property {Array} html - Found information from html files.
     * @property {Array} bower - Found information from bower.json files.
     * @property {Array} icons - Found icons from image files.
     */
    var collectedInfo = {
        package: fileTypes.package.length > 0 ? searchInfo(fileTypes.package, searchInfoPackage) : [],
        html: fileTypes.html.length > 0 ? searchInfo(fileTypes.html, searchInfoHtml) : [],
        bower: fileTypes.bower.length > 0 ? searchInfo(fileTypes.bower, searchInfoPackage) : [],
        icons: fileTypes.images.length > 0 ? searchIcons(fileTypes.images) : [],
        files: [fileTypes.found]
    };

    /**
     * All collected information. 
     * High priority ---> Low priority.
     * Found in file paths -> Package.json -> Index.html -> Other html -> Bower.json
     * @type {Array}
     */
    var allInfo = [].concat(_toConsumableArray(collectedInfo.files), _toConsumableArray(collectedInfo.package), _toConsumableArray(collectedInfo.html), _toConsumableArray(collectedInfo.bower)).reverse();

    /**
     * Merged collected information
     * 
     * @type {Object}
     */
    var mergedInfo = {};

    /** Merge all info objects into one by priority */
    allInfo.forEach(function (info) {
        for (var key in info) {
            var el = mergedInfo[key];
            var newEl = info[key];

            if (!newEl) continue;

            if (mergedInfo.hasOwnProperty(key) && Array.isArray(el) && Array.isArray(newEl)) {
                /** Merge Arrays */
                mergedInfo[key] = [].concat(_toConsumableArray(mergedInfo[key]), _toConsumableArray(newEl));
            } else {
                mergedInfo[key] = newEl;
            }
        }
    });

    /** Remove duplicates from arrays */
    for (var key in mergedInfo) {
        if (Array.isArray(mergedInfo[key])) {
            mergedInfo[key] = uniqArr(mergedInfo[key]);
        }
    }

    mergedInfo.icons = collectedInfo.icons;

    return mergedInfo;
};