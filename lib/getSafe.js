"use strict";

/**
 * Get smth safely with try/catch
 * 
 * @function getSafe
 * @param {Function} fn - Getter.
 * @param {*} defaultVal - Default value if error.
 * @return {*}
 */
module.exports = function (fn, defaultVal) {
    try {
        return fn();
    } catch (e) {
        return defaultVal;
    }
};