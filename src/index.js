const recursive = require('recursive-readdir');
const detectInfo = require('./detectInfo');

const infort = async (projectPath) => {

    /**
     * Non-project files.
     * https://github.com/github/gitignore/blob/master/Node.gitignore
     * 
     * @type {Array}
     */
    let excludeFiles = ['node_modules', 'bower_components', 'jspm_packages', 'logs', 'npm-debug.log*', 'yarn-debug.log*', 'yarn-error.log*', 'pids','*.pid','*.seed','*.pid.lock', 'lib-cov', 'coverage', '.nyc_output', '.grunt', '.lock-wscript', 'typings', '.npm', '.eslintcache', '.node_repl_history', '*.tgz', '.yarn-integrity', '.env', '.next', '.vuepress/dist', '.serverless', 'bin'];

    /**
     * Recursive list of project file paths.
     * @type {Array}
     */
    let files = await recursive(projectPath, excludeFiles);

    /**
     * Information about the project.
     * 
     * @type {Object}
     * @property {(String | null)} name - Name of the project.
     * @property {(String | null)} description - Description of the project.
     * @property {(String | null)} keywords - Keywords of the project.
     * @property {(String | null)} version - Version of the project.
     * @property {(String | null)} icon - Icon of the project.
     */
    let info = detectInfo(files);

    return info;
}

module.exports = infort;