/**
 * Remove duplicates (including json objects) from array.
 * 
 * @function uniqArr
 * @param {Array} arr - Array with duplicates
 * @return {Array} - Unique array
 */
module.exports = (arr) => {
    let set = new Set(arr.map(item => JSON.stringify(item)));
    let dedup = [...set].map(item => JSON.parse(item));

    return dedup;
}