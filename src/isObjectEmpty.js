/**
 * Check if object has only null properties
 * 
 * @function isObjectEmpty
 * @param {Object} obj - Object.
 * @return {*}
 */
module.exports = obj => {
    return Object.values(obj).every(x => !x) || Object.keys(obj).every(key => !obj.hasOwnProperty(key));
}