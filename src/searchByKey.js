/**
 * Search for objects by key.
 * 
 * Object example: 
 * {
 *     "Name": {
 *          ...
 *          "key": "key word" {(String | Array)}
 *      }
 * }
 * 
 * @function searchByKey
 * @param {Object} obj - Data for searching.
 * @param {String} searchKey - Search key.
 * @return {Object}
 * @property {String} name Name of found object
 * @property {String} url Url of found object
 * @property {(String | Array)} key
 */
module.exports = (obj, searchKey) => {
    for (let key in obj) {
        let item = obj[key];
        
        /** Check if key equals searchKey or find it in an Array */
        if ((typeof item.key === 'string' && item.key == searchKey) || (Array.isArray(item.key) && item.key.includes(searchKey))) {
            /** Add the name and remove the key to matched */
            // let newItem = item;
            // newItem.name = key;
            // delete newItem.key;

            return {'url': item.url, 'name': key};
        }
    }
}