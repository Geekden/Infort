const uniqArr = require('./uniqArr');

const sortFiles = require('./sortFiles');

const searchInfo = require('./searchInfo');
const searchInfoHtml = require('./searchInfoHtml');
const searchInfoPackage = require('./searchInfoPackage');
const searchIcons = require('./searchIcons');


/**
 * Collects all information about the project.
 * 
 * @function detectInfo
 * @param {Array} files - Paths of project files.
 * @return {Object} Full information about the project.
 */
module.exports = (files) => {
    /**
     * Sorted files. It is necessary not to read unnecessary files. Optimization.
     * 
     * @type {Object}
     */
    let fileTypes = sortFiles(files);

    /**
     * Сollected information. If object is empty or files not found, return empty array.
     * 
     * @type {Object}
     * @property {Array} package - Found information from package.json files.
     * @property {Array} html - Found information from html files.
     * @property {Array} bower - Found information from bower.json files.
     * @property {Array} icons - Found icons from image files.
     */
    let collectedInfo = {
        package: fileTypes.package.length > 0 ? searchInfo(fileTypes.package, searchInfoPackage) : [],
        html: fileTypes.html.length > 0 ? searchInfo(fileTypes.html, searchInfoHtml) : [],
        bower: fileTypes.bower.length > 0 ? searchInfo(fileTypes.bower, searchInfoPackage) : [],
        icons: fileTypes.images.length > 0 ? searchIcons(fileTypes.images) : [],
        files: [fileTypes.found]
    };

    /**
     * All collected information. 
     * High priority ---> Low priority.
     * Found in file paths -> Package.json -> Index.html -> Other html -> Bower.json
     * @type {Array}
     */
    let allInfo = [...collectedInfo.files, ...collectedInfo.package, ...collectedInfo.html, ...collectedInfo.bower].reverse();


    /**
     * Merged collected information
     * 
     * @type {Object}
     */
    let mergedInfo = {};

    /** Merge all info objects into one by priority */
    allInfo.forEach(info => {
        for (let key in info) {
            let el = mergedInfo[key];
            let newEl = info[key];

            if (!newEl) continue;
            
            if (mergedInfo.hasOwnProperty(key) && Array.isArray(el) && Array.isArray(newEl)) {
                /** Merge Arrays */
                mergedInfo[key] = [...mergedInfo[key], ...newEl];
            } else {
                mergedInfo[key] = newEl;
            }

        }
    });

    /** Remove duplicates from arrays */
    for(let key in mergedInfo) {
        if(Array.isArray(mergedInfo[key])) {
            mergedInfo[key] = uniqArr(mergedInfo[key]);
        }
    }

    mergedInfo.icons = collectedInfo.icons;

    return mergedInfo;
};