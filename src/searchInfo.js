const fs = require('fs');
const path = require('path');
const isObjectEmpty = require('./isObjectEmpty');

/**
 * Processing files.
 * 
 * @function searchInfo
 * @param {Array} files - Files with information.
 * @param {Function} process - File processing function for searching information.
 * @return {Array} Сollected information from files.
 */
module.exports = (files, process) => {

    /**
     * Collected information from files
     * 
     * @type {Array}
     */
    let collectedInfo = files.map((filePath) => {
        /**
         * Data package.
         * @type {String}
         */
        let data = fs.readFileSync(filePath, 'utf-8');

        /** Search information from file */
        return process(data, path.dirname(filePath));
    });

    /** Remove objects without any information */
    collectedInfo = collectedInfo.filter(obj => !isObjectEmpty(obj));

    return collectedInfo;
}